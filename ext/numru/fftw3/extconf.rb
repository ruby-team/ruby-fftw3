require "mkmf"
require "rubygems" unless defined?(Gem)

dir_config('fftw3','/usr/local')

require "rbconfig"
so = RbConfig::CONFIG["DLEXT"]

raise("Your gem is too old") unless Gem.respond_to?(:find_files) 

env = ENV['NARRAY_TYPE']
if env == "NArray" || env == "narray"
  na_type = "narray"
elsif env == "NumRu::NArray" || env == "numru-narray"
  na_type = "numru-narray"
elsif !env.nil?
  raise "Unsupported value for the environment variable NARRAY_TYPE: #{env}"
else
  if Gem.find_files("narray.h").length > 0
    na_type = "narray"
  elsif Gem.find_files("numru/narray/narray.h").length > 0
    na_type = "numru-narray"
  end
end

case na_type
when "narray"
  narray_include = File.expand_path(File.dirname(Gem.find_files("narray.h")[0]))
  narray_lib = File.expand_path(File.dirname(Gem.find_files("narray." + so)[0]))
when "numru-narray"
  narray_include = File.expand_path(File.dirname(Gem.find_files("numru/narray/narray.h")[0]))
  narray_lib = File.expand_path(File.dirname(Gem.find_files("numru/narray/narray." + so)[0]))
end

dir_config('narray', narray_include, narray_lib)
if ( ! ( have_header("narray.h") && have_header("narray_config.h") ) ) then
   print <<-EOS
   ** configure error **  
   Header narray.h or narray_config.h is not found. If you have these files in 
   /narraydir/include, try the following:

   % ruby extconf.rb --with-narray-include=/narraydir/include

EOS
   exit(-1)
end

if ( ! ( have_header("fftw3.h") && have_library("fftw3") ) ) then
   print <<EOS
   ** configure error **
   Header fftw3.h or the compiled fftw3 library is not found.
   If you have the library installed under /fftw3dir (that is, fftw3.h is
   in /fftw3dir/include and the library in /fftw3dir/lib/),
   try the following:

   % ruby extconf.rb --with-fftw3-dir=/fftw3dir

   Alternatively, you can specify the two directory separately
   with --with-fftw3-include and --with-fftw3-lib.
EOS
   exit(-1)
end

if have_library("fftw3f")
  $CFLAGS += ' -DFFTW3_HAS_SINGLE_SUPPORT'
end

create_makefile("numru/fftw3/fftw3")
