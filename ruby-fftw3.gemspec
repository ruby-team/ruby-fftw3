# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'numru/fftw3/version'

filelist = %w!ChangeLog .ChangeLog.until20110419 Gemfile LICENSE.txt Rakefile ruby-fftw3.gemspec!
filelist.concat(Dir::glob("ext/**/*.c\0ext/**/*.rb\0lib/**/*.rb\0test/**/*.rb"))

Gem::Specification.new do |spec|
  spec.name          = "ruby-fftw3"
  spec.version       = NumRu::FFTW3::VERSION
  spec.authors       = ["Takeshi Horinouchi"]
  spec.email         = ['eriko@gfd-dennou.org']

  spec.summary          = %q{The Ruby interface of the FFTW (ver 3) library}
  spec.description      = %q{The Ruby interface of the FFTW3, Fast Fourier Transform library FFTW Ver.3. Since ver.1, ruby-fftw3 supports all kinds of FFTs in FFTW3. Works with NArray (or NumRu::NArray).}
  spec.homepage         = 'http://www.gfd-dennou.org/arch/ruby/products/ruby-fftw3/'
  spec.licenses         = ["BSD-2-Clause"]

  spec.files            = filelist
  spec.test_files       = spec.files.grep(%r{^test/})
  spec.require_paths = ["ext","lib"]

  spec.required_ruby_version = Gem::Requirement.new(">= 1.8")
  spec.add_runtime_dependency(%q<narray>, [">= 0"])

  spec.extensions << "ext/numru/fftw3/extconf.rb"
end
