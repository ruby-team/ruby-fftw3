begin
require "rubygems"
rescue LoadError
end
require "narray"  # This line is needed for rake test when making a gem package.
require "numru/fftw3"
require "test/unit"
include NumRu

class FFTW3Test < Test::Unit::TestCase
  def setup
    @eps = 1e-10
    @seps = 1e-6
  end

  def test_fft_fw_bk
    na = NArray.float(8,4).fill(1)  # will be corced to complex
    na[1,1]=5
    fc = FFTW3.fft_fw(na)
    nb = FFTW3.fft_bk(fc).real
    assert( (na-nb).abs.max < @eps )

    fc = FFTW3.fft_fw(na,1)
    nb = FFTW3.fft_bk(fc,1).real
    assert( (na-nb).abs.max < @eps )
  end

  def test_real_all_dims
    na = NArray.float(8,4).fill(1)  # will be corced to complex
    na[1,1]=5
    fc = FFTW3.fft(na, FFTW3::FORWARD)/na.length 
    nb = FFTW3.fft(fc, FFTW3::BACKWARD).real
    assert( (na-nb).abs.max < @eps )
  end

  def test_complex_all_dims
    na = NArray.complex(8,4).fill(1) * Complex::I
    na[1,1]=5
    fc = FFTW3.fft(na, -1)/na.length 
    nb = FFTW3.fft(fc, 1)
    assert( (na-nb).abs.max < @eps )
  end

  def test_dim_selection
    na = NArray.float(8,4).indgen!
    fc = FFTW3.fft(na, FFTW3::FORWARD, 0)
    fc = FFTW3.fft(fc, FFTW3::FORWARD, 1)
    fc2 = FFTW3.fft(na, FFTW3::FORWARD)
    assert( (fc-fc2).abs.max < @eps )
  end

  # TEST: single float (treated as single if lib fftw3f exits).
  # see http://www.fftw.org/fftw3_doc/Precision.html for more info
  def test_single_float
    na = NArray.sfloat(8,4).indgen!
    fc = FFTW3.fft(na, -1)/na.length 
    nb = FFTW3.fft(fc, 1).real
    assert( (na-nb).abs.max < @seps )
  end
end


